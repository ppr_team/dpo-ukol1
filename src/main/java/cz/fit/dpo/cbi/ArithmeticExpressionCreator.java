package cz.fit.dpo.cbi;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.ArithmeticExpressionCreationDirector;
import cz.fit.dpo.cbi.arithmetic.builder.ArithmeticBuilder;
import cz.fit.dpo.cbi.arithmetic.builder.PostOrderArithmeticBuilder;

public class ArithmeticExpressionCreator {
    

    /**
     * Creates any expression from the RPN input. 
     *
     * @see http://en.wikipedia.org/wiki/Reverse_Polish_notation
     *
     * @param input in Reverse Polish Notation
     * @return {@link ArithmeticExpression} equivalent of the RPN input.
     */
    public ArithmeticExpression createExpressionFromRPN(String input) {
        // TODO: Good entry point for Builder :)

	    ArithmeticBuilder arithmeticBuilder = new PostOrderArithmeticBuilder();
	    ArithmeticExpressionCreationDirector arithmeticExpressionCreationDirector
			    = new ArithmeticExpressionCreationDirector(arithmeticBuilder);
	    arithmeticExpressionCreationDirector.createFromString(input);

	    return arithmeticBuilder.getResult();
    }
}
