package cz.fit.dpo.cbi.arithmetic.tokenizer;

/**
 * Created by kofee on 29.10.14.
 */
public class Token
{
	private TokenType tokenType;
	private Integer integerValue = null;

	public enum TokenType
	{
		NUMBER, ADD_OPERATION, SUBSTRACT_OPERATION, INVALID
	}

	public Token(TokenType tokenType)
	{
		this.tokenType = tokenType;
	}

	public TokenType getTokenType()
	{
		return tokenType;
	}

	public Integer getIntegerValue()
	{
		return integerValue;
	}

	public void setIntegerValue(Integer integerValue)
	{
		this.integerValue = integerValue;
	}
}
