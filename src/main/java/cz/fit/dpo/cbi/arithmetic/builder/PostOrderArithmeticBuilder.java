package cz.fit.dpo.cbi.arithmetic.builder;

import java.util.Stack;

import cz.fit.dpo.cbi.arithmetic.AddOperator;
import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.NumericOperand;
import cz.fit.dpo.cbi.arithmetic.SubstractOperator;

/**
 * Created by kofee on 29.10.14.
 */
public class PostOrderArithmeticBuilder extends ArithmeticBuilder
{
	private Stack<ArithmeticExpression> expressionStack = new Stack<>();

	@Override
	public ArithmeticExpression getResult()
	{
		if(expressionStack.empty())
		{
			throw new IllegalArgumentException("Invalid syntax.");
		}
		return expressionStack.pop();
	}

	@Override
	public void buildNumber(Integer value)
	{
		expressionStack.add(new NumericOperand(value));
	}

	@Override
	public void buildAddOperation()
	{
		ArithmeticExpression right = expressionStack.pop();
		ArithmeticExpression left = expressionStack.pop();
		expressionStack.add(new AddOperator(left, right));
	}

	@Override
	public void buildSubstractOperation()
	{
		ArithmeticExpression right = expressionStack.pop();
		ArithmeticExpression left = expressionStack.pop();
		expressionStack.add(new SubstractOperator(left, right));
	}
}
