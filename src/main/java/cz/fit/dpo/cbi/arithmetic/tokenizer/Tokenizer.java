package cz.fit.dpo.cbi.arithmetic.tokenizer;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by kofee on 29.10.14.
 */
public class Tokenizer implements Iterator<Token>
{
	private String str;
	private int position = 0;

	public Tokenizer(String str)
	{
		this.str = str;
	}

	@Override
	public boolean hasNext()
	{
		boolean has = position < str.length();

		if(has)
		{
			char c = str.charAt(position);
			if(isWhitespace(c))
			{
				eatWhitespace();
			}
			else
			{
				return has;
			}

			return hasNext();
		}

		return has;
	}

	@Override
	public Token next()
	{
		if(!hasNext())
			return null;

		char c = str.charAt(position);

		if(isNumber(c))
		{
			Token token = new Token(Token.TokenType.NUMBER);
			token.setIntegerValue(readNumber());
			return token;
		}

		if(isAddOperation(c))
		{
			eatAddOperation();
			return new Token(Token.TokenType.ADD_OPERATION);
		}

		if(isSubstractOperation(c))
		{
			eatSubstractOperation();
			return new Token(Token.TokenType.SUBSTRACT_OPERATION);
		}

		return new Token(Token.TokenType.INVALID);
	}

	private final boolean isAddOperation(char c)
	{
		return c == '+';
	}

	private final boolean isSubstractOperation(char c)
	{
		return c == '-';
	}

	private final boolean isNumber(char c)
	{
		return c >= '0' && c <= '9';
	}

	private final boolean isWhitespace(char c)
	{
		return c == ' ' || c == '\n' || c == '\r' || c == '\t';
	}

	private final void eatAddOperation()
	{
		position++;
	}

	private final void eatSubstractOperation()
	{
		position++;
	}

	private final Integer readNumber()
	{
		String resStr = "";
		while(position < str.length() && isNumber(str.charAt(position)))
		{
			resStr += str.charAt(position);
			position++;
		}

		return Integer.valueOf(resStr);
	}

	private final void eatWhitespace()
	{
		while(position < str.length() && isWhitespace(str.charAt(position)))
		{
			position++;
		}
	}
}
