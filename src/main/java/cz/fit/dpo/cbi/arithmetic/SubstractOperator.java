package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.SubstractOperation;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;

import java.util.Iterator;

/**
 * Represents - operation
 */
public class SubstractOperator extends BinaryOperator {
    
    public SubstractOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand)
    {
        super(firstOperand, secondOperand);
    }    

    @Override
    public Integer evaluate()
    {
        return ((ArithmeticExpression)(getFirstOperand())).evaluate()
                -((ArithmeticExpression)(getSecondOperand())).evaluate();
    }

    @Override
    public ExpressionElement getElement()
    {
        return new SubstractOperation();
    }

}
