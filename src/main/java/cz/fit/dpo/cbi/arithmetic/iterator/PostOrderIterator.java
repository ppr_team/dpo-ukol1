package cz.fit.dpo.cbi.arithmetic.iterator;

import java.util.Iterator;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.BinaryOperator;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;

public class PostOrderIterator implements Iterator<ExpressionElement> {

    BinaryOperator next;
    Iterator<ExpressionElement> left;
    Iterator<ExpressionElement> right;
    boolean hasNext;

    public PostOrderIterator(BinaryOperator root)
    {
        this.next = root;
        left = ((ArithmeticExpression)next.getFirstOperand()).getPostOrderIterator();
        right = ((ArithmeticExpression)next.getSecondOperand()).getPostOrderIterator();
        hasNext = true;
    }

    @Override
    public boolean hasNext()
    {
        return hasNext;
    }

    @Override
    public ExpressionElement next()
    {
        if(left.hasNext())
            return left.next();
        else if(right.hasNext())
            return right.next();
        else
        {
            hasNext = false;
            return next.getElement();
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }
}
