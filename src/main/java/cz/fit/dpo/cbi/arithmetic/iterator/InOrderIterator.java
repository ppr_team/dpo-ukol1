package cz.fit.dpo.cbi.arithmetic.iterator;

import java.util.Iterator;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.BinaryOperator;
import cz.fit.dpo.cbi.arithmetic.elements.CloseBracketOperation;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.elements.OpenBracketOperation;

public class InOrderIterator implements Iterator<ExpressionElement> {

    BinaryOperator next;
    Iterator<ExpressionElement> left;
    Iterator<ExpressionElement> right;
    boolean takeRoot;
	boolean touchedLeft = false;
	boolean finishRight = false;

    public InOrderIterator(BinaryOperator root)
    {
        this.next = root;
        left = ((ArithmeticExpression)next.getFirstOperand()).getInOrderIterator();
        right = ((ArithmeticExpression)next.getSecondOperand()).getInOrderIterator();
        takeRoot = true;
    }

    @Override
    public boolean hasNext()
    {
        return (left.hasNext() || takeRoot || right.hasNext()) || finishRight;
    }

    @Override
    public ExpressionElement next()
    {
		if(finishRight)
		{
			finishRight = false;
			return new CloseBracketOperation();
		}
        if(left.hasNext())
        {
	        if(!touchedLeft)
	        {
		        touchedLeft = true;
		        return new OpenBracketOperation();
	        }
	        return left.next();
        }
        else
        {
	        if (takeRoot)
	        {
		        takeRoot = false;
		        return next.getElement();
	        }
	        else
	        {
		        ExpressionElement ret = right.next();

		        if(!right.hasNext())
		        {
			        finishRight = true;
		        }

		        return ret;
	        }
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }
}
