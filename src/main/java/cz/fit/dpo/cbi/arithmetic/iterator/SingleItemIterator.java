package cz.fit.dpo.cbi.arithmetic.iterator;

import cz.fit.dpo.cbi.arithmetic.NumericOperand;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by frantisek on 29.10.14.
 */
public class SingleItemIterator implements Iterator<ExpressionElement>
{

    NumericOperand next;

    public SingleItemIterator(NumericOperand item)
    {
        this.next = item;
    }

    @Override
    public boolean hasNext()
    {
        return next!=null;
    }

    @Override
    public ExpressionElement next()
    {
        if(!hasNext())
            throw  new NoSuchElementException();

        ExpressionElement ret = next.getElement();
        next = null;

        return ret;
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }
}
