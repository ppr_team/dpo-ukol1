package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;

import java.util.Iterator;

/**
 * Represents the Binary operations like + - etc.
 */
public abstract class BinaryOperator extends ArithmeticExpression {

    private ArithmeticExpression firstOperand;
    private ArithmeticExpression secondOperand;

    public BinaryOperator(ArithmeticExpression firstOperand, ArithmeticExpression secondOperand)
    {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }
    
    public Object getFirstOperand() {
        return firstOperand;
    }

    public Object getSecondOperand() {
        return secondOperand;
    }

    @Override
    public Iterator<ExpressionElement> getInOrderIterator()
    {
        return new InOrderIterator(this);
    }

    @Override
    public Iterator<ExpressionElement> getPostOrderIterator()
    {
        return new PostOrderIterator(this);
    }

}
