package cz.fit.dpo.cbi.arithmetic.builder;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;

/**
 * Created by kofee on 29.10.14.
 */
public abstract class ArithmeticBuilder
{
	public abstract ArithmeticExpression getResult();

	public abstract void buildNumber(Integer value);
	public abstract void buildAddOperation();
	public abstract void buildSubstractOperation();
}
