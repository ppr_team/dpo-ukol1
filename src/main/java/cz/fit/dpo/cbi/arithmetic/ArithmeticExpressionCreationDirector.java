package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.builder.ArithmeticBuilder;
import cz.fit.dpo.cbi.arithmetic.tokenizer.Token;
import cz.fit.dpo.cbi.arithmetic.tokenizer.Tokenizer;

/**
 * Created by kofee on 29.10.14.
 */
public class ArithmeticExpressionCreationDirector
{
	private ArithmeticBuilder builder;

	public ArithmeticExpressionCreationDirector(ArithmeticBuilder builder)
	{
		this.builder = builder;
	}

	public void createFromString(String str)
	{
		Tokenizer tokenizer = new Tokenizer(str);

		while(tokenizer.hasNext())
		{
			Token token = tokenizer.next();

			switch (token.getTokenType())
			{
				case NUMBER:
					builder.buildNumber(token.getIntegerValue());
					break;
				case ADD_OPERATION:
					builder.buildAddOperation();
					break;
				case SUBSTRACT_OPERATION:
					builder.buildSubstractOperation();
					break;
				case INVALID:
					throw new IllegalArgumentException("Invalid token.");

			}
		}
	}
}
