package cz.fit.dpo.cbi.arithmetic;

import cz.fit.dpo.cbi.arithmetic.elements.*;
import cz.fit.dpo.cbi.arithmetic.elements.Number;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.PostOrderIterator;
import cz.fit.dpo.cbi.arithmetic.iterator.SingleItemIterator;

import java.util.Iterator;

/**
 * Represents number in the arithmetic expression
 */
public class NumericOperand extends ArithmeticExpression {

    private Integer value;

    public NumericOperand(Integer value) {
        this.value = value;
    }

    @Override
    public Integer evaluate() {
        return value;
    }

    @Override
    public Iterator<ExpressionElement> getInOrderIterator() {
        return new SingleItemIterator(this);
    }

    @Override
    public Iterator<ExpressionElement> getPostOrderIterator() {
        return new SingleItemIterator(this);
    }

    @Override
    public ExpressionElement getElement()
    {
        return new Number(value);
    }

}
