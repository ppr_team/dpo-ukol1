package cz.fit.dpo.cbi;

import cz.fit.dpo.cbi.arithmetic.ArithmeticExpression;
import cz.fit.dpo.cbi.arithmetic.elements.ExpressionElement;
import cz.fit.dpo.cbi.arithmetic.iterator.InOrderIterator;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Printer for {@link ArithmeticExpression}s. It can print inOrder notation (3 + 1) or postOrder
 * notation (3 1 +).
 *
 * PostOrder is RPN (Reverse Polish Notation) in fact. See wiki for more information.
 *
 */
public class ArithmeticExpressionPrinter
{
    private ArithmeticExpression expression;

    public ArithmeticExpressionPrinter(ArithmeticExpression expression) {
        this.expression = expression;
    }

    /**
     * Print an expression in classical notation, e.g. (3+1).     
     * The "(" and ")" is used to preserve priorities.     
     * @return String in classical "inOrder" format.
     */
    public String printInOrder()
    {
        StringBuilder bld = new StringBuilder();

        Iterator<ExpressionElement> it = expression.getInOrderIterator();

        while(it.hasNext())
        {
            bld.append(it.next().stringValue());
        }

        return bld.toString();
    }    

    /**
     * Print an expression in RPN notation, e.g. 3 1 +.     
     * Please note, the "(" and ")" are no longer necessary, because RPN does not need them.
     * @return string in "postOrder" (RPN) format.
     */
    public String printPostOrder()
    {
        StringBuilder bld = new StringBuilder();

        Iterator<ExpressionElement> it = expression.getPostOrderIterator();

        while(it.hasNext())
        {
            bld.append(it.next().stringValue());
	        bld.append(" ");
        }
	    if(bld.length() > 0)
	    {
		    bld.deleteCharAt(bld.length() - 1);
	    }

        return bld.toString();
    }

}
